import sys
import pandas as pd
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity

# funcion para combinar features
def combinar_features(row):
 return row['keywords']+' '+row['cast']+' '+row['genres']+' '+row['director']

# funcion para obtener el titulo de la pelicula a partir del index
def get_title_from_index(index):
    return df[df.index == index]['title'].values[0]
# funcion para obtener el index de la pelicula a partir del titulo
def get_index_from_title(title):
	try: 
		return df[df.title == title]['index'].values[0]
	except:
		print 'La pelicula no existe en la base de datos'
		return -1

# se lee el dataset de peliculas
df = pd.read_csv('./movies_database/movie_dataset.csv')

features = ['keywords','cast','genres','director']

# se preprocesa la informacion de las caracteristicas para eliminar los NAN
for feature in features:
    df[feature] = df[feature].fillna('') 

# se crea columna nueva con la combinacion de las caracteristicas
df['combined_features'] = df.apply(combinar_features, axis=1)
#print "Combined Features:", df["combined_features"].head()

# se crea el objeto CountVectorizer()
cv = CountVectorizer()

# se crea una matriz con las caracteristicas combinadas 
count_matrix = cv.fit_transform(df['combined_features']) 

# se obtiene la funcion de similaridad
cosine_sim = cosine_similarity(count_matrix)

# pelicula a analizar
movie_user_likes = str(sys.argv[1])

# se obtiene el index de la pelicula
movie_index = get_index_from_title(movie_user_likes)
if movie_index == -1:
	sys.exit()

# se calcula el puntaje de similaridad de la pelicula deseada con todas las peliculas de la base de datos
similarity_scores = list(enumerate(cosine_sim[movie_index]))

# se orden los puntajes de similaridad en orden descendente
sorted_similar_movies = sorted(similarity_scores,key=lambda x:x[1],reverse=True)[1:]

# se obtiene el top 5 de peliculas similares
i=0
print 'El top 5 de peliculas similares a '+movie_user_likes+ ' son:\n'
for element in sorted_similar_movies:
    print(get_title_from_index(element[0]))
    i=i+1
    if i>4:
    	break
